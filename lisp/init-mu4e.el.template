;;; package --- mu4e
;;; Commentary:
;;; Code:

(add-to-list 'load-path "/usr/share/emacs/site-lisp/elpa-src/mu4e-1.10.8")
(require 'mu4e)


(autoload 'mu4e "mu4e" "Mail client based on mu (maildir-utils)." t)

(setq
 mu4e-maildir (expand-file-name "~/.Maildir")
 mu4e-drafts-folder "/Drafts"
 mu4e-sent-folder   "/Sent"
 mu4e-trash-folder  "/Trash")

;;(setq mu4e-get-mail-command "offlineimap")

;; http://cachestocaches.com/2017/3/complete-guide-email-emacs-using-mu-and-/
;; https://www.djcbsoftware.nl/code/mu/mu4e/Bookmarks.html
;; Include a bookmark to open all of my inboxes
(add-to-list 'mu4e-bookmarks
             '(:name "All Inboxes"
              :query "maildir:/INBOX"
              :key ?i))
(add-to-list 'mu4e-bookmarks
             '(:name "Gmail unread"
              :query "maildir:/Gmail AND flag:unread AND NOT flag:trashed"
              :key ?g))
;; This allows me to use 'helm' to select mailboxes
(setq mu4e-completing-read-function 'completing-read)
;; Why would I want to leave my message open after I've sent it?
(setq message-kill-buffer-on-exit t)
;; Don't ask for a 'context' upon opening mu4e
(setq mu4e-context-policy 'pick-first)
;; Don't ask to quit... why is this the default?
(setq mu4e-confirm-quit nil)

(setq mu4e-contexts
      `( ,(make-mu4e-context
           :name "Private"
           :enter-func (lambda () (mu4e-message "Entering Private context"))
           :leave-func (lambda () (mu4e-message "Leaving Private context"))
           ;; we match based on the contact-fields of the message
           :match-func (lambda (msg)
                         (when msg
                           (mu4e-message-contact-field-matches msg
                                                               '(:to :from :cc :bcc)
                                                               ".*@home.example.com")))
           :vars '( ( user-mail-address	    . "aliced@home.example.com"  )
                    ( user-full-name	    . "ßingen" )
                    ( mu4e-compose-signature . nil)
                    ( mu4e-sent-folder . "/Sent")
                    ( user-mail-address . "aliced@home.example.com")
                    ( smtpmail-smtp-user . "aliced@home.example.com")
                    ( smtpmail-local-domain . "example.com")
                    ( smtpmail-default-smtp-server . "smtp.example.com")
                    ( smtpmail-smtp-server . "smtp.example.com")
                    ( smtpmail-smtp-service . 587)
                    ( smtpmail-stream-type . starttls)
                    ( smtpmail-use-gnutls . t)))
         ,(make-mu4e-context
           :name "Work"
           :enter-func (lambda () (mu4e-message "Switch to the Work context"))
           ;; no leave-func
           ;; we match based on the maildir of the message
           ;; this matches maildir /Arkham and its sub-directories
           :match-func (lambda (msg)
                         (when msg
                           (string-match-p "^/Arkham" (mu4e-message-field msg :maildir))))
           :vars '( ( user-mail-address	     . "aderleth@miskatonic.example.com" )
                    ( user-full-name	    . "ßingen" )
                    ( mu4e-compose-signature . nil)                    (mu4e-sent-folder . "/Sent")
                    ( user-mail-address . "aliced@home.example.com")
                    ( smtpmail-smtp-user . "aliced@home.example.com")
                    ( smtpmail-local-domain . "example.com")
                    ( smtpmail-default-smtp-server . "smtp.example.com")
                    ( smtpmail-smtp-server . "smtp.example.com")
                    ( smtpmail-smtp-service . 587)
                    ( smtpmail-stream-type . starttls)
                    ( smtpmail-use-gnutls . t)))

         ,(make-mu4e-context
           :name "Cycling"
           :enter-func (lambda () (mu4e-message "Switch to the Cycling context"))
           ;; no leave-func
           ;; we match based on the maildir of the message; assume all
           ;; cycling-related messages go into the /cycling maildir
           :match-func (lambda (msg)
                         (when msg
                           (string= (mu4e-message-field msg :maildir) "/cycling")))
           :vars '( ( user-mail-address	     . "aderleth@example.com" )
                    ( user-full-name	    . "ßingen" )
                    ( mu4e-compose-signature  . nil)
                    ( mu4e-sent-folder . "/Sent")
                    ( user-mail-address . "aliced@home.example.com")
                    ( smtpmail-smtp-user . "aliced@home.example.com")
                    ( smtpmail-local-domain . "example.com")
                    ( smtpmail-default-smtp-server . "smtp.example.com")
                    ( smtpmail-smtp-server . "smtp.example.com")
                    ( smtpmail-smtp-service . 587)
                    ( smtpmail-stream-type . starttls)
                    ( smtpmail-use-gnutls . t)))))

;; set `mu4e-context-policy` and `mu4e-compose-policy` to tweak when mu4e should
;; guess or ask the correct context, e.g.

;; start with the first (default) context;
;; default is to ask-if-none (ask when there's no context yet, and none match)
;; (setq mu4e-context-policy 'pick-first)

;; compose with the current context is no context matches;
;; default is to ask
;; (setq mu4e-compose-context-policy nil)

;; http://pragmaticemacs.com/emacs/master-your-inbox-with-mu4e-and-org-mode/
;;store org-mode links to messages
(require 'mu4e-org)
;;store link to message if in header view, not to header query
(setq org-mu4e-link-query-in-headers-mode nil)

(provide 'init-mu4e)

;;; init-mu4e.el ends here
