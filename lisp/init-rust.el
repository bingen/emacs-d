;;; package --- Rust mode
;;; Commentary:
;;; Code:

;; https://manenko.com/2016/08/03/setup-emacs-for-rust-development.html

;;(require 'init-elpa)
(require-package 'company)
(require-package 'racer)
(require-package 'rust-mode)
(require-package 'flycheck)
(require-package 'flycheck-rust)

(require 'company)
(require 'racer)
(require 'rust-mode)
(require 'electric)
(require 'eldoc)
(require 'flycheck)
(require 'flycheck-rust)

(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))
(add-hook 'rust-mode-hook  #'company-mode)
(add-hook 'rust-mode-hook  #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
(add-hook 'rust-mode-hook
          '(lambda ()
             (setq racer-cmd (concat (getenv "HOME") "/.rust-dev/racer/target/release/racer"))
             (setq racer-rust-src-path (concat (getenv "HOME") "/.rust-dev/rust/src"))
             (local-set-key (kbd "TAB") #'company-indent-or-complete-common)
             (electric-pair-mode 1)))

;; rustfmt
(setq rust-format-on-save t)

(provide 'init-rust)

;;; init-rust.el ends here
