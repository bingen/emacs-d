;;; package --- Dumb-Jump
;;; Commentary:
;;; Code:

(require-package 'dumb-jump)

(dumb-jump-mode)

(provide 'init-dumb-jump)

;;; init-dumb-jump.el ends here
