;;; package --- ox-gfm
;;; Commentary: org conversion to GitHub flavored Markdown
;;; Code:

(require-package 'ox-gfm)

(eval-after-load "org"
  '(require 'ox-gfm nil t))

(provide 'init-ox-gfm)

;;; init-ox-gfm.el ends here
