;;; package --- Haskell mode
;;; Commentary:
;;; Code:

(require-package 'haskell-mode)

;; https://emacsclub.github.io/html/haskell.html#sec-3
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

;; https://gitlab.haskell.org/ghc/ghc/wikis/emacs
;; Highlight text beyond 80th column
(require 'whitespace)
(defun haskell-custom-line-length ()
  (setq whitespace-style '(face lines-tail))
  (setq whitespace-line-column 80)
  (global-whitespace-mode t))

(add-hook 'haskell-mode-hook 'haskell-custom-line-length)

;; Allows you to jump to the first error in compiled or interpreted code.
(global-set-key (kbd "M-g M-f") 'first-error)

(provide 'init-haskell)

;;; init-haskell.el ends here
