;;; package --- Summary
;;; Commentary:
;;; Code:

(require-package 'vyper-mode)

(provide 'init-vyper)

;;; init-vyper.el ends here
