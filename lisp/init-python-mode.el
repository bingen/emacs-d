;;; package --- Summary
;;; Commentary:
;;; Code:
(setq auto-mode-alist
      (append '(("SConstruct\\'" . python-mode)
		("SConscript\\'" . python-mode))
              auto-mode-alist))

(require-package 'pip-requirements)

;; See: https://github.com/purcell/emacs.d/issues/724
(setq python-shell-interpreter "python3")

(setq-default python-indent 4)
(setq-default python-indent-offset 4)

(provide 'init-python-mode)
;;; init-python-mode.el ends here
