;;; package --- themes
;;; Commentary:
;;; Code:

(when (< emacs-major-version 24)
  (require-package 'color-theme))

;;(require-package 'color-theme-sanityinc-solarized)
;;(require-package 'color-theme-sanityinc-tomorrow)
(require-package 'base16-theme)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;; (load-theme 'tron-legacy t)
;; (load-theme 'base16-tomorrow t)
;; (load-theme 'base16-tomorrow-night t)
(load-theme 'base16-harmonic16-light t)

;; If you don't customize it, this is the theme you get.
;;(setq-default custom-enabled-themes '(sanityinc-solarized-light))

;; Ensure that themes will be applied even if they have not been customized
(defun reapply-themes ()
  "Forcibly load the themes listed in `custom-enabled-themes'."
  (dolist (theme custom-enabled-themes)
    (unless (custom-theme-p theme)
      (load-theme theme)))
  (custom-set-variables `(custom-enabled-themes (quote ,custom-enabled-themes))))

(add-hook 'after-init-hook 'reapply-themes)

;;------------------------------------------------------------------------------
;; Toggle between light and dark
;;------------------------------------------------------------------------------
(defun light ()
  "Activate a light color theme."
  (interactive)
;;  (color-theme-sanityinc-solarized-light))
  (load-theme 'base16-harmonic16-light t))

(defun dark ()
  "Activate a dark color theme."
  (interactive)
;;  (color-theme-sanityinc-solarized-dark))
  (load-theme 'base16-harmonic16-dark t))


(provide 'init-themes)
