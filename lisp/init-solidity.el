;;; package --- Solidity mode
;;; Commentary:
;;; Code:

(require-package 'solidity-mode)
(require-package 'solidity-flycheck)
(require-package 'company-solidity)

;; https://emacs.stackexchange.com/a/17565/12560
(defun solidity-custom-settings ()
  (setq indent-tabs-mode nil)
  (setq tab-width 4)
  (setq c-basic-offset 4))

(add-hook 'solidity-mode-hook 'solidity-custom-settings)

(setq c-basic-offset 4)

;; https://github.com/ethereum/emacs-solidity
;(define-key map (kbd "C-c C-g") 'solidity-estimate-gas-at-point)

(setq solidity-solc-path "/usr/local/bin/solc-static-linux-0.4.24")
(setq solidity-solium-path "~/.nvm/versions/node/v9.4.0/bin/solium")

(setq solidity-flycheck-solc-checker-active t)
(setq solidity-flycheck-solium-checker-active t)

(setq flycheck-solidity-solc-addstd-contracts t)
(setq flycheck-solidity-solium-soliumrcfile "~/workspace/aragonOS/.soliumrc.json")

(add-hook 'solidity-mode-hook
          (lambda ()
            (set (make-local-variable 'company-backends)
                 (append '((company-solidity company-capf company-dabbrev-code))
                         company-backends))))


;; https://gitlab.haskell.org/ghc/ghc/wikis/emacs
(require 'whitespace)
(defun solidity-custom-line-length ()
  (setq whitespace-style '(face lines-tail))
  (setq whitespace-line-column 145)
  (global-whitespace-mode t))

(add-hook 'solidity-mode-hook 'solidity-custom-line-length)


;; See this issue about indentation and general solidity style:
;; https://github.com/ethereum/emacs-solidity/issues/32
;; Create ethereum c/c++ style
(defconst ethereum-c-style
  '((c-tab-always-indent        . t)
    ;; always syntactically analyze and indent macros
    (c-syntactic-indentation-in-macros . t)
    ;; auto align backslashes for continuation
    (c-auto-align-backslashes . t)
    (c-comment-only-line-offset . 4)
    (c-hanging-braces-alist     . ((substatement-open after)
                                   (brace-list-open)))
    (c-hanging-colons-alist     . ((member-init-intro before)
                                   (inher-intro)
                                   (case-label after)
                                   (label after)
                                   (access-label after)))
    (c-cleanup-list             . (scope-operator
                                   empty-defun-braces
                                   defun-close-semi))
    (c-offsets-alist            . (
                                   ;; lineup the current argument line under the
                                   ;; first argument
                                   ;; (arglist-close . c-lineup-arglist)
                                   (arglist-close . 0) ;; <-- or not
                                   (arglist-cont-nonempty . 4)
                                   (substatement-open . 0)
                                   (case-label        . 0)
                                   (label             . 0)
                                   (block-open        . 4)
                                   ;; Don't indent first brace of a class's function
                                   (inline-open       . 0)
                                   ;; all opens should be indented
                                   (brace-list-open   . 4)
                                   ;; indent after case
                                   (statement-case-intro   . 4)
                                   ;; indent after entering a block
                                   (statement-block-intro   . 4)
                                   ;; indent after entering a function definition
                                   (defun-block-intro   . 4)
                                   ;; indent when entering class/struct
                                   (inclass   . 4)
                                   ;; don't indent the first line in a topmost construct
                                   (topmost-intro . 0)
                                   ;; first line after if/while/for/do/else
                                   (substatement . 4)
                                   ;; don't indent when in extern scope
                                   (inextern-lang . 0)
                                   ;; when ; does not end a stmt do indent
                                   (statement-cont . 4)
                                   ;; c++ constructor member initi no indent
                                   (member-init-intro . 4)
                                   (member-init-cont . 4)
                                   ;; don't indent c++ namespace
                                   (innamespace . 0)
                                   ;; don't indent first comments
                                   (comment-intro . 0)
                                   ))
    ;; echo syntactic info when presing TAB. If `t' helps with debugging.
    (c-echo-syntactic-information-p . t))
  "Ethereum C/C++ programming style.")
(c-add-style "ethereum" ethereum-c-style)

(defun ethereum-c-mode-hook ()
  "C/C++ hook for ethereum development."
  (subword-mode 1) ;all word movement goes across Capitalized words
;;  (setq-default c-indent-tabs-mode t)     ; tabs instead of spaces
;;  (setq-default indent-tabs-mode t)       ; tabs instead of spaces
;;  (setq-default c-indent-level 4)         ; A TAB is equivalent to 4 spaces
;;  (setq-default c-basic-offset 4)
;;  (setq-default tab-width 4)              ; Default tab width is 4
  (setq-default c-argdecl-indent 0)       ; Do not indent argument decl's extra
  (setq-default c-tab-always-indent t)
;;  (setq-default backward-delete-function nil) ; DO NOT expand tabs when deleting
  (c-set-style "ethereum"))

(add-hook 'solidity-mode-hook 'ethereum-c-mode-hook)

(provide 'init-solidity)
;;; init-solidity.el ends here
