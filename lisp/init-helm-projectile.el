;;; package --- Helm Projectile
;;; Commentary:
;;; Code:

;;; https://github.com/bbatsov/helm-projectile

(require-package 'helm-projectile)

(helm-projectile-on)

(provide 'init-helm-projectile)
;;; init-helm-projectile ends here
