;;; package --- literate-calc-mode
;;; Commentary:
;;; Code:

(require-package 'literate-calc-mode)


(provide 'init-literate-calc)

;;; init-literate-calc.el ends here
