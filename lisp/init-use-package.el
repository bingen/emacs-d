;;; package --- use-package macro
;;; Commentary:
;;; Code:
;;; https://github.com/jwiegley/use-package

(require-package 'use-package)

(provide 'init-use-package)

;;; init-use-package.el ends here
