;;; package --- pandoc-mode
;;; Commentary: Emacs mode for interacting with Pandoc
;;; Code:

(require-package 'pandoc-mode)


(provide 'init-pandoc)

;;; init-pandoc.el ends here
